import axios from "axios";
import store from "../store";
import router from "../router";
const TokenService = store.getters["auth/getToken"];
const ApiService = {
  // Stores the 401 interceptor position so that it can be later ejected when needed
  _401interceptor: null,
  imgURL: "",
  init(baseURL) {
    this.baseURL = baseURL;
    axios.defaults.baseURL = baseURL;
    this.mount401Interceptor();
  },
  setImgURL(imgURL) {
    this.imgURL = imgURL;
  },
  getImgURL() {
    return this.imgURL;
  },
  setHeader(token) {
    if (token)
      //When it logs in
      axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    //When it loads the token from the app mount
    else
      axios.defaults.headers.common["Authorization"] = `Bearer ${TokenService}`;
    console.log("Setting header");
    console.log(`Bearer ${TokenService}`);
  },
  removeHeader() {
    axios.defaults.headers.common = {};
  },

  get(resource) {
    return axios.get(resource);
  },

  post(resource, data) {
    return axios.post(resource, data);
  },

  put(resource, data) {
    return axios.put(resource, data);
  },

  delete(resource) {
    return axios.delete(resource);
  },
  /**
   * Perform a custom Axios request.
   *
   * data is an object containing the following properties:
   *  - method
   *  - url
   *  - data ... request payload
   *  - auth (optional)
   *    - username
   *    - password
   **/
  customRequest(data) {
    return axios(data);
  },

  /**
   * When the response from the server is 401 it means that the JWT has expired and the user needs to login
   */
  mount401Interceptor() {
    this._401interceptor = axios.interceptors.response.use(
      response => {
        return response;
      },
      error => {
        if (error.request.status == 401) {
          console.log(error);

          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          store.dispatch("auth/AUTH_LOGOUT");
          router.push("/login");
          throw error;
        }

        // If error was not 401 just reject as is
        throw error;
      }
    );
  },

  unmount401Interceptor() {
    // Eject the interceptor
    axios.interceptors.response.eject(this._401interceptor);
  }
};

export default ApiService;
