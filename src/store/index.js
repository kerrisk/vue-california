import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import { auth } from "./modules/auth.module";
import { cart } from "./modules/cart.module";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment(state) {
      state.count++;
    }
  },
  actions: {},
  modules: {
    auth: auth,
    cart: cart
  },
  getters: {
    getCount: state => {
      return state.count;
    }
  },
  plugins: [createPersistedState()]
});
