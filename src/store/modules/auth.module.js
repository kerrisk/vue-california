import router from "../../router";
import ApiService from "../../services/ApiService";
import store from "../../store";

const state = {
  token: "",
  status: "",
  userData: {}
};

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  getToken: state => state.token,
  getUserData: state => state.userData,
  getNombre: state => state.userData.nombre,
  getProfilePic: state =>
    ApiService.getImgURL() +
    state.userData.profilePic.src.replace(/^.+?[/]/, "")
};

const actions = {
  AUTH_REQUEST: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      // The Promise used for router redirect in login
      commit("AUTH_REQUEST");
      ApiService.post("/auth/login", user)
        .then(resp => {
          const token = resp.data.data.token;
          const userData = resp.data.data;
          console.log(resp);
          commit("AUTH_SUCCESS", { token, userData });
          ApiService.setHeader(token);
          //Tries to load saved cart
          ApiService.get("/cart", user)
            .then(resp => {
              console.log(resp);
              store.dispatch("cart/CART_LOAD_PROD", resp.data.data.productos);
              // you have your token, now log in your user :)
              //dispatch("USER_REQUEST");
              //resolve(resp);
            })
            .catch(err => {
              console.log(err);
              console.log("Cart Get Error");
              //reject(err);
            });
          // you have your token, now log in your user :)
          //dispatch("USER_REQUEST");
          resolve(resp);
        })
        .catch(err => {
          commit("AUTH_ERROR", err);
          reject(err);
        });
    });
  },
  AUTH_LOGOUT: ({ commit }) => {
    commit("AUTH_LOGOUT");
    ApiService.removeHeader();
    //ApiService.unmount401Interceptor();
    //router.push("/Dashboard");
  },
  AUTH_UPDATE: ({ commit }, formData) => {
    return new Promise((resolve, reject) => {
      console.log(formData);
      ApiService.customRequest({
        method: "post",
        url: "/auth/update",
        data: formData,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          const userData = response.data.data;
          console.log(response);
          commit("AUTH_UPDTSUCC", { userData });
          console.log(response.data);
          resolve(response);
        })
        .catch(error => {
          console.log(error.config);
          reject(error);
        });
    });
  },
  AUTH_DELETE_ACC: ({ commit }) => {
    return new Promise((resolve, reject) => {
      ApiService.delete("/auth/")
        .then(response => {
          console.log(response);
          commit("AUTH_DELETE_ACC");
          console.log(response.data);
          resolve(response);
        })
        .catch(error => {
          console.log(error.config);
          reject(error);
        });
    });
  }
};

const mutations = {
  AUTH_REQUEST: state => {
    state.status = "loading";
  },
  AUTH_SUCCESS: (state, payload) => {
    const { token, userData } = { ...payload };
    state.status = "success";
    state.token = token;
    state.userData = userData;
  },
  AUTH_UPDTSUCC: (state, payload) => {
    const { userData } = payload;
    state.status = "success";
    state.userData = userData;
  },
  AUTH_ERROR: state => {
    state.status = "error";
    state.token = "";
  },
  AUTH_LOGOUT: state => {
    state.status = "success";
    state.token = "";
    store.dispatch("cart/CART_CLEAN_PROD");
  },
  AUTH_DELETE_ACC: () => {
    store.dispatch("auth/AUTH_LOGOUT");
    router.push({ path: "/" });
  }
};

export const auth = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
