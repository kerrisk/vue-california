import ApiService from "../../services/ApiService";
const state = {
  productos: [],
  ordenes: [],
  plantillas: [],
  productosEdit: [],
  total: ""
};

const getters = {
  getProductos: state => state.productos,
  getProdLen: state => state.productos.length,
  getTotal: state => state.total,
  getOrders: state => state.ordenes,
  getProductosEdit: state => state.productosEdit
};

const mutations = {
  CART_LOAD_PROD: (state, productos) => {
    state.productos = [...state.productos, ...productos];
  },
  CART_ADD_PROD: (state, producto) => {
    if (state.productos.length > 0) state.productos.push(producto);
    else state.productos = [producto];
    console.log(state.productos);
    ApiService.post("/cart", {
      productos: state.productos
    })
      .then(response => {
        // handle success
        console.log("Cart Saved correctly " + new Date());
        console.log(response);
        console.log(this.productos);
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  },
  CART_REMOVE_PROD: (state, producto) => {
    const index = state.productos.indexOf(producto);
    if (index > -1) state.productos.splice(index, 1);
    console.log(state.productos);
    ApiService.post("/cart", {
      productos: state.productos
    })
      .then(response => {
        // handle success
        console.log("Cart Saved correctly " + new Date());
        console.log(response);
        console.log(this.productos);
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  },
  CART_CLEAN_PROD: state => {
    state.productos = [];
  },
  ORDERS_GET: state => {
    ApiService.get("/order")
      .then(response => {
        // handle success
        console.log(response);
        state.ordenes = response.data.data;
        console.log(response);
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  },
  ORDERS_DELETE: (state, order) => {
    const index = state.ordenes.indexOf(order);
    if (index > -1) state.ordenes.splice(index, 1);
  },
  ORDERS_EDIT: (state, payload) => {
    const { oldOrder, newOrder } = payload;
    const index = state.ordenes.indexOf(oldOrder);
    if (index > -1) state.ordenes.splice(index, 1, newOrder);
  },
  PRODUCTS_GET: state => {
    ApiService.get("/product")
      .then(response => {
        // handle success
        state.productosEdit = response.data.data.map(val => {
          val.picture.src =
            ApiService.getImgURL() + val.picture.src.replace(/^.+?[/]/, "");
          return val;
        });
        console.log(response);
      })
      .catch(error => {
        // handle error
        console.log(error);
      });
  },
  PRODUCTS_DELETE: (state, product) => {
    const index = state.productosEdit.indexOf(product);
    if (index > -1) state.productosEdit.splice(index, 1);
  },
  PRODUCTS_ADD: (state, product) => {
    product.picture.src =
      ApiService.getImgURL() + product.picture.src.replace(/^.+?[/]/, "");
    state.productosEdit.push(product);
  },
  PRODUCTS_EDIT: (state, payload) => {
    const { oldproduct, newproduct } = payload;
    newproduct.picture.src =
      ApiService.getImgURL() + newproduct.picture.src.replace(/^.+?[/]/, "");
    const index = state.productosEdit.indexOf(oldproduct);
    if (index > -1) state.productosEdit.splice(index, 1, newproduct);
  }
};

const actions = {
  CART_LOAD_PROD: ({ commit }, productos) => {
    commit("CART_LOAD_PROD", productos);
  },
  CART_ADD_PROD: ({ commit }, producto) => {
    commit("CART_ADD_PROD", producto);
  },
  CART_REMOVE_PROD: ({ commit }, producto) => {
    commit("CART_REMOVE_PROD", producto);
  },
  CART_CLEAN_PROD: ({ commit }) => {
    commit("CART_CLEAN_PROD");
  },
  ORDERS_GET: ({ commit }) => {
    commit("ORDERS_GET");
  },
  ORDERS_DELETE: ({ commit }, order) => {
    commit("ORDERS_DELETE", order);
  },
  ORDERS_EDIT: ({ commit }, payload) => {
    commit("ORDERS_EDIT", payload);
  },
  PRODUCTS_GET: ({ commit }) => {
    commit("PRODUCTS_GET");
  },
  PRODUCTS_DELETE: ({ commit }, product) => {
    commit("PRODUCTS_DELETE", product);
  },
  PRODUCTS_ADD: ({ commit }, product) => {
    commit("PRODUCTS_ADD", product);
  },
  PRODUCTS_EDIT: ({ commit }, payload) => {
    commit("PRODUCTS_EDIT", payload);
  }
};

export const cart = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
