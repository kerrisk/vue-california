import Vue from "vue";
import store from "../store";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/Dashboard.vue")
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/profile",
    name: "Profile",
    component: () =>
      import(/* webpackChunkName: "profile" */ "../views/Profile.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue")
  },
  {
    path: "/store",
    name: "Store",
    component: () =>
      import(/* webpackChunkName: "store" */ "../views/Store.vue")
  },
  {
    path: "/orders",
    name: "Orders",
    component: () =>
      import(/* webpackChunkName: "orders" */ "../views/Orders.vue")
  },
  {
    path: "/ordersAdmin",
    name: "OrdersAdmin",
    component: () =>
      import(/* webpackChunkName: "ordersAdmin" */ "../views/OrdersAdmin.vue")
  },
  {
    path: "/AddProduct",
    name: "AddProduct",
    component: () =>
      import(/* webpackChunkName: "addproduct" */ "../views/AddProduct.vue")
  },
  {
    path: "/AddPaq",
    name: "AddPaq",
    component: () =>
      import(/* webpackChunkName: "addpaq" */ "../views/AddPaq.vue")
  },
  {
    path: "/templates",
    name: "Templates",
    component: () =>
      import(/* webpackChunkName: "templates" */ "../views/Templates.vue")
  },
  {
    path: "/DashAdm",
    name: "DashAdm",
    component: () =>
      import(/* webpackChunkName: "dashadm" */ "../views/DashboardAdm.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
    meta: { requiresNotAuth: true }
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/Register.vue"),
    meta: { requiresNotAuth: true }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters["auth/isAuthenticated"]) {
      next({ path: "/login", query: { redirect: to.fullPath } });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresNotAuth)) {
    if (store.getters["auth/isAuthenticated"]) {
      next({ path: "/", query: { redirect: to.fullPath } });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});
export default router;
