# Dashboard Cliente
![Product Page](https://s3.envato.com/files/201915864/preview/01-homepage-v1.jpg)
## Con sidebar como la del dashboard del empleado
- Home
- Pedidos
- Plantillas
- Mi Cuenta
- Metodos de pago
- Configuracion
- Acerca de
- Log out

## Contenido del dashboard del cliente
- Slider Paquetes
- 3 cards que se redirigen a Zonas de la tienda(Paquetes, Pasillos, Ofertas)
- Nuevos Productos
- Componente ( Ofertas, Mas Vendidos, Recomendados)

##Icono Carrito (parte superior Derecha)


# Pagina Tienda
![Store](https://s3.envato.com/files/245191128/06_Preview.jpg)
##TabBar (Pasillos, Paquetes, Ofertas)

###Pasillos tendra tabBar anidado
- Frutas
- Verduras
- Cereales
- Otros

# Página Carrito
![Car Page](https://file.mockplus.com/image/2019/05/849be009-85e1-4093-936a-f53e15345aeb.png)

- ListView productos Carrito
- CheckBox Añadir a Plantillas Personalizadas
- Cupones
- Metodo de Pago
- Info del Pago (Subtotal, Descuento, Total, etc)


# Dashboard Empleados
![Dashboard](https://cdn.dribbble.com/users/2566888/screenshots/8965279/media/a548db50c9bb3df0e54333e6e39966cf.png)

# API Mongo
## Productos
