# vue-california
`vue 2.6.11`
## Project installation
Clone the repository or download the zip file and unpack
```bash
git clone git@bitbucket.org:kerrisk/vue-california.git
```
## Project setup
### To run it
```
cd vue-california
npm install
npm run serve
```
### Setting up environments

1.  You will find a file named `env.example` on root directory of project.
2.  Create a new file by copying and pasting the file and then renaming it to `.env`
    ```bash
    cp .env.example .env.development
    ```
3.  The file `.env` is already ignored, so you never commit your credentials.
4.  Change the values of the file to your environment. Helpful comments added to `.env.example` file to understand the constants.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm install --global prettier
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
